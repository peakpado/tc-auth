/**
 * Copyright (c) 2014 TopCoder, Inc. All rights reserved.
 */
/**
 * Provides methods for checking permission.
 *
 * @author peakpado
 * @version 1.0
 */
'use strict';


var _ = require('lodash');
var errors = require('common-errors');
var checkAuth = require('./checkAuth');

/**
 * @param config the app configuration object
 */
module.exports = function(config) {

  /**
   * get the permissions from configuration
   */
  function getPerms() {
    var perms = [];
    if (config && config.auth && config.auth.perms) {
      perms = config.auth.perms;
    } else {
      perms = [
        'challengeApp'
      ];
    }

    // If from config and this is not an array then split on comma
    if (!_.isArray(perms)) {
      perms = perms.split(',');
    }
    return perms;
  }

  /**
   * Check whether current user has a permission or not
   * @param req the request
   */
  function currentUserPass(req) {
    var perms = getPerms();
    var user = checkAuth.getSigninUser(req);

    var permitted = perms.every(function (perm) {
      if (user.perms[perm]) {
        return true;
      } else {
        return false;
      }
    });
    return permitted;
  }

  /**
   * Middleware that checkes user permission.
   * @param req the request
   * @param res the response
   * @param next the next
   */
  function checkPerms(req, res, next) {
    if (!currentUserPass(req)) {
      // Remove user and tcUser This will cause requireAuth middleware to return
      // @TODO Later we might want to log this info so we shouldn't be deleting it
      delete req.user;
      delete req.tcUser;
    }
    next();
  };

  return {
    getPerms: getPerms,
    checkPerms: checkPerms,
    currentUserPass: currentUserPass
  }
}