/**
 * Copyright (c) 2014 TopCoder, Inc. All rights reserved.
 */
/**
 * Provides methods for checking and getting safe list.
 *
 * @author peakpado
 * @version 1.0
 */
'use strict';


var _ = require('lodash');
var checkAuth = require('./checkAuth');

/**
 * @param config the app configuration object
 */
module.exports = function(config) {

  /**
   * Middleware that checkes safelist.
   * @param req the request
   * @param res the response
   * @param next the next
   */
  function safeList(req, res, next) {
    if (config && config.auth && config.auth.safeList && config.auth.safeList.enabled) {
      if (!currentUserSafe(req)) {
        // Remove user and tcUser This will cause requireAuth middleware to return
        // @TODO Later we might want to log this info so we shouldn't be deleting it
        delete req.user;
        delete req.tcUser;
      }
    }
    next();
  };

  /**
   * Get the safe list.
   */
  function getUserSafeList() {
    var safeListUsers = [];
    if (config && config.auth && config.auth.safeList && config.auth.safeList.users) {
      safeListUsers = config.auth.safeList.users;
    } else {
      safeListUsers = [
        'dayal',
        'rockabilly',
        'kbowerma',
        ' _indy',
        'appiriowes'
      ];
    }

    // If from config and this is not an array then split on comma
    if (!_.isArray(safeListUsers)) {
      safeListUsers = safeListUsers.split(',');
    }
    return safeListUsers;
  }

  /**
   * Check current user is in the safe list or not.
   * @param req the request
   */
  function currentUserIsSafe(req) {
    return getUserSafeList().indexOf(checkAuth.getSigninUser(req));
  }

  return {
    safeList: safeList,
    getUserSafeList: getUserSafeList,
    currentUserIsSafe: currentUserIsSafe
  }
}