/**
 * Copyright (c) 2014 TopCoder, Inc. All rights reserved.
 */
/**
 * Provides methods for checking authentication.
 *
 * @author peakpado
 * @version 1.0
 */
'use strict';


var errors = require('common-errors');

/**
 * Authentication handler for authenticated paths defined in configuration settings
 * @param req the request
 * @param res the response
 * @param next the next
 */
exports.requireAuth = function(req, res, next) {
  if(!req.user || !req.tcUser) {
    return next(new errors.AuthenticationRequiredError("User is not authenticated"));
  }
  next();
};

/**
 * Return the user currently singed in.
 * @param req the request
 *
 * tcUser object
 * {
 *  "id": 40015014,
 *  "handle": "indy_qa3",
 *  "full_name": "Neil Hastings",
 *  "avatar_url": "//www.gravatar.com/avatar/b56ee7ec472676dfda65dd62bde2ff3e"
 *  "perms": {
 *   "challengeApp": true
 *  }
 * }
 */
exports.getSigninUser = function(req) {
  return req.tcUser ? req.tcUser : {};
};