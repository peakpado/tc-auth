/**
 * Copyright (c) 2014 TopCoder, Inc. All rights reserved.
 */
/**
 * Provides methods for accessing tc-user service.
 *
 * @author peakpado
 * @version 1.0
 */
'use strict';


var request = require('request');
var errors = require('common-errors');

/**
 * Middleware to get mock tc user.
 * @param req the request
 * @param res the response
 * @param next the next
 */
function mockUser(req, res, next) {
  req.tcUser = {
    id: parseInt(req.headers['x-id']) || 40015014,
    name: req.headers['x-full-name'] || 'Neil Hastings',
    handle: req.headers['x-handle'] || 'indy_qa3',
    picture: req.headers['x-avatar-url'] || '//www.gravatar.com/avatar/b56ee7ec472676dfda65dd62bde2ff3e',
    perms: {
      challengeApp: true
    }
  };
  next();
};

/**
 * @param config the app configuration object
 */
module.exports = function(config) {

  /**
   * Middleware to get tc user from tc-user-service.
   * @param req the request
   * @param res the response
   * @param next the next
   */
  function tcUser(req, res, next) {

    if (!config.app || !config.app.tcUser) {
      next(new errors.ValidationError('app.tcUser is not configured. Set the URL of tc-user service to app.tcUser.'));
    }

    if (req.user) {
      var options = {
        url: config.app.tcUser + '/user',
        headers: {
          'Authorization': req.get('Authorization'),
          'x-id': req.get('x-id'),
          'x-full-name': req.get('x-full-name'),
          'x-handle': req.get('x-handle'),
          'x-avatar-url': req.get('x-avatar-url')
        }
      };

      request(options, function (error, response, body) {
        if (!error && response.statusCode === 200) {
          body = JSON.parse(body);

          req.tcUser = {
            id: body.id,
            name: body.fullName,
            handle: body.handle,
            picture: body.avatarUrl,
            perms: body.perms
          };
          next();
        } else {
          //TODO: handle error response from tc api
          var err = new errors.Error('TC API Unavailable');
          err.statusCode = 503;
          next(err);
        }
      });
    } else {
      next();
    }
  };

  return {
    tcUser: tcUser,
    mockUser: mockUser
  }
}