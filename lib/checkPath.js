/**
 * Copyright (c) 2014 TopCoder, Inc. All rights reserved.
 */
/**
 * Provides methods for checking route path for auth handling.
 *
 * @author peakpado
 * @version 1.0
 */
'use strict';


var _ = require('lodash');

/**
 * Authenticated paths for the application.
 * Configure authPaths in configuration settings
 * authPaths is an array of object. Each object has following structure
 *   {
 *     httpVerb: '<GET/POST/PUT/DELETE/PATCH>',
 *     path: '<SECURED ENDPOINT>'
 *   }
 *   For ex:
 *   {
 *     httpVerb: 'GET',
 *     path: '/challenges/:challengeId/files/:fileId/download'
 *   }
 *
 * @type {Array}
 */


/**
 * @param config the app configuration object
 */
module.exports = function(config) {

  var authPaths = [];
  if (config && config.auth && config.auth.paths) {
    authPaths = config.auth.paths;
  }
  var routingMethods = {
    GET: 'get',
    POST: 'post',
    PUT: 'put',
    DELETE: 'delete',
    HEAD: 'head',
    OPTIONS: 'options'
  };

  /**
   * Install auth middlewares based on the authPaths configuration
   * @param app the express app
   * @param mw middleware the auth middle ware to add to the path
   */
  function checkPath(app, mw) {
    // adding auth handler for file download and upload ENDPOINTS defined in configuration settings
    if (_.isArray(authPaths)) {
      _.forEach(authPaths, function(authPath) {
        var verb = routingMethods[authPath.httpVerb];
        if (verb) {
          app[verb](authPath.path, mw);
        }
      });
    }
  }

  return {
    checkPath: checkPath
  };
};

