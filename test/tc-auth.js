/**
 * Copyright (c) 2014 TopCoder, Inc. All rights reserved.
 */
/**
 * Test middlewares.
 *
 * @author peakpado
 * @version 1.0
 */
'use strict';


var express = require('express');
var request = require('supertest');
var assert = require('assert');
var should = require('should');
var errors = require('common-errors');
var nock = require('nock');
var jwt = require('jsonwebtoken');
var testHelper = require('./test-helper');


var tcAuth = require('../');


describe('tc-auth module', function () {
  var app;
  beforeEach(function (done) {
    app = express();
    done();
  });

  describe('auth disabled', function () {
    it('should access api successfully without authentication', function (done) {
      var config = {
        auth: {
          enabled: false
        }
      };

      tcAuth.auth(app, config);

      app.use('/api', function (req, res) {
        assert(req.tcUser);
        assert.equal(req.tcUser.name, 'Neil Hastings');
        res.status(200).end();
      });

      request(app)
      .get('/api')
      .expect(200, done);
    });
  });

  describe('auth enabled', function () {

    it('should access api with valid auth header and permission', function (done) {

      var config = testHelper.authEnabledConfig;
      var tcUser = testHelper.tcUser;
      tcAuth.auth(app, config);

      // mock tc-user request with nock
      var scope = nock(config.app.tcUser)
                .get('/user')
                .reply(200, tcUser);
      // create jwt token
      var token = jwt.sign({id: tcUser.id, aud: config.auth0.client}, config.auth0.passphrase);

      app.use('/api', function (req, res) {
        res.status(200).end();
      });
      app.use(testHelper.errorHandler);

      request(app)
      .get('/api')
      .set('Authorization', 'Bearer '+token)
      .expect(200, done);
    });

    it('should throw exception without auth0 client and secret configuration', function (done) {
      var config = {
        auth: {
          enabled: true
        }
      };

      assert.throws(function () {
          tcAuth.auth(app, config);
        },
        errors.ValidationError
      );
      done();
    });

    it('should fail to access APIs without auth header', function (done) {

      tcAuth.auth(app, testHelper.authEnabledConfig);

      app.use('/api', function (req, res) {
        assert(false, 'Authentication should fail before reaching controller logic');
        res.status(200).end();
      });
      app.use(testHelper.errorHandler);

      request(app)
      .get('/api')
      .expect(401)
      .end(function (err, res) {
        res.body.result.success.should.be.false;
        res.body.result.status.should.equal(401);
        res.body.content.should.equal('No Authorization header was found');
        done();
      });
    });

    it('should fail to access APIs without permission', function (done) {

      var config = testHelper.authEnabledConfig;
      var tcUserNoPerms = testHelper.tcUserNoPermission;
      tcAuth.auth(app, config);

      // mock tc-user request with nock
      var scope = nock(config.app.tcUser)
                .get('/user')
                .reply(200, tcUserNoPerms);
      // create jwt token
      var token = jwt.sign({id: tcUserNoPerms.id, aud: config.auth0.client}, config.auth0.passphrase);

      app.use('/api', function (req, res) {
        res.status(200).end();
      });
      app.use(testHelper.errorHandler);

      request(app)
      .get('/api')
      .set('Authorization', 'Bearer '+token)
      .expect(401)
      .end(function (err, res) {
        res.body.result.success.should.be.false;
        res.body.result.status.should.equal(401);
        res.body.content.should.equal('An attempt was made to perform an operation without authentication: User is not authenticated');
        done();
      });
    });

  });
  

});

