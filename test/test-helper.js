/**
 * Copyright (c) 2014 TopCoder, Inc. All rights reserved.
 */
/**
 * Helper methods and objects for testing.
 *
 * @author peakpado
 * @version 1.0
 */
'use strict';

var errors = require('common-errors');


// simple error handler for testing
exports.errorHandler = function(err, req, res, next) {
  if (err) {
    var httpError = new errors.HttpStatusError(err);
    res.status(httpError.statusCode).json({
      result: {
        success: false,
        status: httpError.statusCode
      },
      content: err.message
    });
  } else {
    next();
  }
};

exports.tcUser = {
  id: 20015014,
  fullName: 'Neil Hastings',
  handle: 'indy_qa3',
  avatarUrl: '//www.gravatar.com/avatar/b56ee7ec472676dfda65dd62bde2ff3e',
  perms: {
    challengeApp: true
  }
};

exports.tcUserNoPermission = {
  id: 40015014,
  fullName: 'Neil Hastings',
  handle: 'indy_qa3',
  avatarUrl: '//www.gravatar.com/avatar/b56ee7ec472676dfda65dd62bde2ff3e',
  perms: {}
};

exports.authEnabledConfig = {
  app: {
    tcUser: 'http://localhost:1234'
  },
  auth: {
    enabled: true,
    perms: ['challengeApp'],
    paths: [{
      httpVerb: 'GET',
      path: '*'
    }]
  },
  auth0: {
    client: 'tc-client',
    secret: 'c2VjcmV0',
    passphrase: 'secret'
  }
};