tc-auth
===========

The tc-auth library is implemented based on [Json Web Token](https://github.com/auth0/node-jsonwebtoken) and [Express-JWT](https://github.com/auth0/express-jwt), authenticate user with JWT token in Authorization header.

## Installation
```npm install tc-auth```


## Auth Middlewares
* ***jwtCheck***: This is `express-jwt` module which decodes the JWT token in the Authorization header, populate it in `req.user`. The JWT should have user id data that will be sent to tc1-user-service.
* ***tcUser***: Retrieve tc-user from tc1-user-service and populate it in `req.tcUser`.
* ***checkPerms***: Check whether a user has the permission on the given service. The permission check is done for all configured permissions against req.tcUser.perms.
* ***checkAuth***: Check whether req.user or req.tcUser is populated or not. If not present, throws `AuthenticationRequiredError` error.

## Usage
Require tc-auth module.

	var tcAuth = require('tc-auth');

### Auth Disaled
When auth is disable, no authentication check is performed, so all requests pass the auth middlewares, reaches to API routes.

	var config = {
		auth: {
			enabled: false
		}
	}
	
	tcAuth.auth(app, config);

### Auth Enabled
To enable auth, app.tcUser, auth and auth0 configuration are requried. Below is a sample configuration.

	var config = {
	  app: {
	    tcUser: 'http://localhost:1234'
	  },
	  auth: {
	    enabled: true,
	    perms: ['challengeApp'],
	    paths: [{
	      httpVerb: 'GET',
	      path: '*'
	    }]
	  },
	  auth0: {
	    client: 'tc-client',
	    secret: 'c2VjcmV0'
	  }
	};
	
	tcAuth.auth(app, config);

## Test
To run the test, type `npm test`.

	$ npm test

## Authors

This library was developed by [peakpado](peakpado@gmail.com) at [TopCoder](http://www.topcoder.com)


## License

Copyright (c) 2014 TopCoder, Inc. All rights reserved.