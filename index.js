/**
 * Copyright (c) 2014 TopCoder, Inc. All rights reserved.
 */
/**
 * Index for tc-auth handling.
 *
 * @author peakpado
 * @version 1.0
 */
'use strict';


var jwtCheck = require('./lib/jwtCheck');
var checkAuth = require('./lib/checkAuth');


/**
 * @param app the express app
 * @param config the app configuration object
 */
function auth(app, config) {
  var authEnabled = false;
  var auth0Config = {};

  if (config) {
    if (config.auth && config.auth.enabled) {
      authEnabled = config.auth.enabled;
    }
    if (config.auth0) {
      auth0Config = config.auth0;
    }
  }

  var checkPaths = require('./lib/checkPath')(config);
  var tcUser = require('./lib/tcUser')(config);
  var checkPerms = require('./lib/checkPerms')(config);


  // First check if auth is enabled
  if (authEnabled && authEnabled !== 'false') {
    // Add the jwt middleware to the paths
    checkPaths.checkPath(app,
      [jwtCheck.jwtCheck(auth0Config), tcUser.tcUser, checkPerms.checkPerms, checkAuth.requireAuth]);

    //TODO Need auth configuration per route. Leave it as it is for now
    app.get('/challenges/:challengeId/register',
      [jwtCheck.jwtCheck(auth0Config), tcUser.tcUser, checkAuth.requireAuth]);
  } else {
    // fake auth
    app.use(tcUser.mockUser);
  }
};


module.exports = {
  safeList: require('./lib/safelist'),
  auth: auth,
  getSigninUser: checkAuth.getSigninUser
}
